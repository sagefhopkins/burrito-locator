#!/usr/bin/env python3

from base64 import b64encode
from os import urandom, getenv
import os

from flask import Flask, render_template, request, session, redirect, url_for
from flask_sqlalchemy import SQLAlchemy
from werkzeug.utils import secure_filename
import geocoder
import csv
from functools import wraps
import json
import requests

# Change the name later
app = Flask('app')

UPLOAD_FOLDER = 'uploads'
# Remove this when production
app.config['SEND_FILE_MAX_AGE_DEFAULT'] = 0
# Change this to reflect the actual production database

db_user = getenv('DB_USER')
db_pass = getenv('DB_PASS')
db_host = getenv('DB_HOST')

app.config['SQLALCHEMY_DATABASE_URI'] = f'mysql+pymysql://{db_user}:{db_pass}@{db_host}/google_Maps'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['UPLOAD_FOLDER'] = UPLOAD_FOLDER
ALLOWED_EXTENSIONS = {'csv'}

db = SQLAlchemy(app)

app.secret_key = b64encode(urandom(256))

from .models import Locations

from .forms import SearchForm
def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in ALLOWED_EXTENSIONS

def get_Lat_Lng(query, state):
    try:
        query = str(query) +'+'+str(state)
        url = 'https://maps.googleapis.com/maps/api/geocode/json'
        para = {'sensor': 'false', 'address': query, 'key': 'AIzaSyCPfUedHCltsv3Lfhk94Auzq6SDbw08YSU'}
        r = requests.get(url, params=para)
        results = r.json()['results']
        location = results[0]['geometry']['location']
        #print(url, para)
        return (location['lat'], location['lng'])
    except:
        pass
        return None


def create_Table(name, street, city, state, country, zipcode, status):
    #print(state)
    if state == None:
        return
    else:
        try:
            lat, lng = get_Lat_Lng(street, state)
            #https://www.google.com/maps/place/2790+N+Powers+Blvd,+Colorado+Springs
            locations = Locations()
            locations.name = name
            locations.street = street
            locations.city = city
            locations.state = state
            locations.country = country
            locations.zipcode = zipcode
            locations.lat = str(lat)
            locations.lng = str(lng)
            directions = 'https://www.google.com/maps/place/' + street.replace(' ', '+') + '+' + city.replace(' ', '+') + '+' + state
            locations.directions = directions
            locations.status = status
            #print(directions)
            db.session.add(locations)
            db.session.commit()
        except:
            pass
def update_Status(uid, status_Update):
    uid = Locations.query.get(uid)
    uid.status = status_Update
    db.session.commit()

def remove_Item(uid):
    uid = Locations.query.get(uid)
    db.session.delete(uid)
    db.session.commit()

def create_Table_From_CSV(file):
    with open(file, mode='rt', encoding='ISO-8859-1') as csv_file:
        line_count = 0
        csv_reader = csv.DictReader(csv_file)
        for row in csv_reader:
            if line_count == 0:
                line_count += 1
            else:
                create_Table(row['name'], row['street'], row['city'], row['state'], row['country'], row['zip'], row['status'])

def city_Find(query):
    if ',' in query:
        city, state = query.split(',')
        return city, state
    else:
        return query, None

def location_Find(query):
    city, state = city_Find(query)
    results = Locations.query.filter_by(status=1, city=city)
    #print(results)
    return results

def unapproved_Locations():
    results = Locations.query.filter_by(status=2)
    return results

def approved_Locations():
    results = Locations.query.filter_by(status=1)
    return results

def is_logged_in(f):
    @wraps(f)
    def wrap(*args, **kwargs):
        if 'logged_in' in session:
            return f(*args, **kwargs)
        else:
            return redirect(url_for('login_dashboard'))
    return wrap



@app.before_first_request
def initialize():
    db.create_all()
    #create_Table_From_CSV('list.csv')


@app.route('/')
def index():
    results = None
    return render_template('index.html', results=results)

@app.route('/search', methods=['POST', 'GET'])
def search():
    query = request.form['search']
    city, state = city_Find(query)
    try:
        lat, lng = get_Lat_Lng(city, state)
        results = location_Find(query)

        return render_template('index.html', lat=lat, lng=lng, results=results)
    except:
        pass
        return redirect(url_for('index'))


@app.route('/request', methods=['POST', 'GET'])
def request_Store():
    if request.method == 'POST':
        name = request.form['name']
        street = request.form['street']
        city = request.form['city']
        state = request.form['state']
        zipcode = request.form['zipcode']
        country = request.form['country']
        create_Table(name, street, city, state, country, zipcode, 2)
    else:
        pass

    return render_template('request.html')

@app.route('/dashboard/login', methods=['POST', 'GET'])
def login_dashboard():
    username = "admin"
    password = "FDS*()%#$FDSVv"
    if request.method == 'POST':
        username_Canidate = request.form['username']
        password_Canidate = request.form['password']
        if password_Canidate == password and username_Canidate == username:
            session['logged_in'] = True
            session['username'] = username_Canidate
            return redirect(url_for('dashboard'))
    else:
        pass

    return render_template('login_Dashboard.html')

@app.route('/dashboard', methods=['POST', 'GET'])
@is_logged_in
def dashboard():
    if request.method =='POST':
        if request.form['action'] == 'approve':
            for item in request.form.getlist("check"):
                update_Status(item[0], 1)
        elif request.form['action'] == 'remove':
            for item in request.form.getlist("check"):
                remove_Item(item[0])
        elif request.form['action'] == 'unapprove':
            for item in request.form.getlist("check"):
                update_Status(item[0], 2)
        elif request.form['action'] == 'bulk':
            return redirect(url_for('bulk_Upload'))
    else:
        pass

    data = unapproved_Locations()
    data_2 = approved_Locations()
    return render_template('dashboard.html', data=data, data_2=data_2)

@app.route('/dashboard/bulk', methods=['POST', 'GET'])
@is_logged_in
def bulk_Upload():
    if request.method == 'POST':
        if 'file' not in request.files:
            #print('No file part')
            return redirect(url_for('bulk_Upload'))
        file = request.files['file']
        if file.filename =='':
            #print('No file')
            return redirect(url_for('bulk_Upload'))
        if file and allowed_file(file.filename):
            filename = secure_filename('list.csv')
            file.save(os.path.join(app.config['UPLOAD_FOLDER'], filename))
            create_Table_From_CSV('uploads/list.csv')
            return redirect(url_for('dashboard'))
    return render_template('bulk.html')
