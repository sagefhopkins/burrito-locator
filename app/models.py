#!/usr/bin/env python3

from . import app, db

from flask_sqlalchemy import SQLAlchemy


class Locations(db.Model):
    __tablename__ = 'locations'
    uid = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=False, nullable=True)
    street = db.Column(db.String(100), unique=False, nullable=False)
    city = db.Column(db.String(120), unique=False, nullable=False)
    state = db.Column(db.String(100), unique=False, nullable=False)
    country = db.Column(db.String(100), unique=False, nullable=False)
    zipcode = db.Column(db.String(100), unique=False, nullable=False)
    lat = db.Column(db.String(100), unique=False, nullable=False)
    lng = db.Column(db.String(100), unique=False, nullable=False)
    directions = db.Column(db.String(200), unique=False, nullable=False)
    status = db.Column(db.Integer, unique=False, nullable=False)

    def __repr__(self):
        return '<User: %r' % self.uid
