#!/usr/bin/env python3

from os import getenv

from . import app

if __name__ == '__main__':
    if getenv('FLASK_ENV') == 'development':
        app.run(debug=True, host='0.0.0.0', port='5000')
    else:
        app.run(debug=False)
